---
title: "Uptime robot&Uptime kuma：网站监控报警软件"
slug: "Uptimerobot"
date: 2022-02-21T14:52:37+08:00
draft: false
description: "配置简答、界面美观"
tags: ['监控','在线服务','自建']
---

站长需要知晓当前网站的运行状况，这里提供三个监控方案以供参考。

![](2022-02-27-17-12-03.png)

## 注册Uptime Robot

[官网](https://uptimerobot.com/)

Uptime Robot是一个网站监控服务，每 5 分钟检查一次你设定的网站 或服务器，最多可以免费检查 50 个网站。

如果你的网站或者服务器宕机时，Uptime Robot会通过邮件提醒你。

后台界面有些丑，但是添加之后可以生成分享界面。分享界面和UptimeKuma形式是一样的。很简洁。

免费版功能受限，并且此平台和Kuma数据不互通（即无法一键导入导出），给我们带来不便。由于Uptime Robot被滥用于某些特殊用途，某些服务已将其服务器访问封禁。

![](2022-02-27-17-17-50.png)

### 使用方法

访问[UptimeRobot: Free Website Monitoring Service](https://uptimerobot.com/)注册账号。

转到dashboard，将你的网页地址复制进去，设置好间隔和通知邮箱即可

## 搭建Uptime Kuma

[在线演示](https://demo.uptime.kuma.pet/)

Uptime Kuma是一个开源的监控工具，功能类似于 Uptime Robot。支持自托管服务，限制更少，支持配置文件备份。

这里提供现成托管服务+VPS自建两个方案。

### 使用PikaPods现成托管服务

[参考本文](https://dev.kermsite.com/post/pikapods/)

### 自己搭建

另一种方案是自己搭建Uptime Kuma

[项目地址](https://github.com/louislam/uptime-kuma)

推荐使用docker一键部署：

```shell
docker volume create uptime-kuma
docker run -d --restart=always -p 3001:3001 -v uptime-kuma:/app/data --name uptime-kuma louislam/uptime-kuma:1
⚠️ Please use a local volume only. Other types such as NFS are not supported.
```

之后访问：http://ip:3001

## Uptime 系列软件的其他用途

### 用于阻止某些服务休眠

okteto、heroku等服务商为我们提供了免费的容器服务。但是也有一些限制。例如长时间不用之后会休眠，再次访问只能重新部署。

已知会休眠的服务：

- heroku：长时间不访问导致休眠，重新部署，550h每月
- okteto：一天一休，重新部署
- glitch：长时间不访问休眠，1000h月

> **注意**：
>
> - glitch阻止了某些网站的访问，所以用Uptimerobot会报错，需要自己搭建。
>
> - heroku不绑定信用卡每个月只有550h的免费额度。如果一直不休眠可能会导致超额。绑定信用卡可升级到1000h。
> - okteto即使使用Robot最多也只能维持一天的数据

参考：
1. https://github.com/louislam/uptime-kuma
2. https://taifua.com/uptimerobot-monitor.html