哪吒监控 一站式轻监控轻运维系统。支持系统状态、HTTP、TCP、Ping 监控报警，计划任务和在线终端。

![](2022-02-25-17-13-28.png)

## 说明

如果你仅仅需要最简单的监控功能，也可以使用serverstatus。这是另外一款VPS探针，和哪吒面板相比，服务端较容易配置，但是监控端需要手动添加，并且功能较少。

本文将简要介绍如何在Caddy环境下配置哪吒面板。

本文以Ubuntu20.14为例，其他系统相应修改Caddy部署过程。

## 配置GitHub

哪吒面板使用GitHub认证系统进行登录。配置好之后，只要将我们的GitHub用户名加入管理员权限，就可以使用GitHub一键登录。

首先需要注册一个GitHub认证app：

转到https://github.com/settings/developers，然后点击New OAuth App按钮。

如下图，如果你使用IP访问面板的话，两个空直接填写

```
http://你的IP:8008
http://你的IP:8008/oauth2/callback
# 加端口
```

即可。我这里是配置了域名访问以及TLS。则填写我访问面板所使用的域名

```
https://status.kermsite.com
https://status.kermsite.com/oauth2/callback
# 不加端口
```

![](2022-02-25-16-45-37.png)

点击绿色按钮创建，转到App，复制Client ID，生成、复制Client secrets：

![](2022-02-25-16-51-10.png)

## 配置主控端程序

需要放行8008、5555两个端口，这是默认的，如果你程序中改为其他的，防火墙放行相应的端口。

使用一键脚本进行部署：

```
# 国外机器
curl -L https://raw.githubusercontent.com/naiba/nezha/master/script/install.sh -o nezha.sh && chmod +x nezha.sh
./nezha.sh
```

```
# 国内机器
curl -L https://raw.sevencdn.com/naiba/nezha/master/script/install.sh -o nezha.sh && chmod +x nezha.sh
CN=true ./nezha.sh
```

输入1回车，选择GitHub（国内相应选择gitee）接着就要输入前面记录下来的Client ID、Client secrets了。然后会设定管理员账号，**输入你的GitHub账号用户名**即可。

可以访问http://域名:8008查看，但此时还不能登录（未配置好回调地址）。

## 配置主控端域名

接下来我们进行域名的绑定以及TLS的开启。

```shell
# 安装Caddy
sudo apt install -y debian-keyring debian-archive-keyring apt-transport-https
curl -1sLf 'https://dl.cloudsmith.io/public/caddy/stable/gpg.key' | sudo tee /etc/apt/trusted.gpg.d/caddy-stable.asc
curl -1sLf 'https://dl.cloudsmith.io/public/caddy/stable/debian.deb.txt' | sudo tee /etc/apt/sources.list.d/caddy-stable.list
sudo apt update
sudo apt install caddy
# 修改Caddyfile
nano /etc/caddy/Caddyfile
```

如下进行配置，将域名修改为你需要绑定的域名

```
status.kermsite.com {
        reverse_proxy localhost:8008
        tls 你的邮箱地址
}
```

进行DNS解析。如果使用cloudflare，**请首先不要点亮小云朵**。

重新启动Caddy

```shell
caddy stop
caddy start
```

稍等一会儿，就可以通过域名访问面板了，点击右上角登录，即可访问后端。此时可以打开小云朵。

## 配置受控端

最新版本的哪吒面板支持一键配置Linux受控端。

如图配置面板服务器IP。

![](2022-02-25-17-07-02.png)

添加主机，如图点击复制，转到受控端运行即可。

![](2022-02-25-17-08-43.png)

## 配置内部传输接口（可选）

可以将内部传输接口也套上域名，**不能点亮小云朵**。

```
statusbk.kermsite.com {
        reverse_proxy localhost:5555
}
```

## 配置报警推送到TG（可选）

### 配置bot

先创建bot，获取userid，然后复制本链接到推送消息界面

```
# https://api.telegram.org/botXXXXXX/sendMessage?chat_id=YYYYYY&text=#NEZHA# 

```

![](2022-02-25-17-23-05.png)

### 配置推送规则

离线通知规则示例：

![](2022-02-25-17-24-34.png)

```
2. 添加一个离线报警
    - 名称：离线通知
    - 规则：`[{"Type":"offline","Min":0,"Max":0,"Duration":10}]`
    - 启用：√
3. 添加一个监控 CPU 持续 10s 超过 50% **且** 内存持续 20s 占用低于 20% 的报警
    - 名称：CPU+内存
    - 规则：`[{"Type":"cpu","Min":0,"Max":50,"Duration":10},{"Type":"memory","Min":20,"Max":0,"Duration":20}]`
    - 启用：√
```



参考：
1. https://haoduck.com/644.html
2. https://www.moewah.com/archives/3794.html
3. https://github.com/naiba/nezha
4. https://blog.v001.ml/2021/05/27/%E5%93%AA%E5%90%92%E9%9D%A2%E6%9D%BF-%E6%B7%BB%E5%8A%A0%E7%9B%91%E6%8E%A7%E5%B9%B6%E9%80%9A%E7%9F%A5telegram/ 添加监控

