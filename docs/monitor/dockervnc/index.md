---
title: "为Linux服务器一键安装vnc远程桌面（基于docker）"
slug: "Dockervnc"
date: 2022-03-05T11:34:18+08:00
draft: false
description: "添加远程桌面最方便的方式"
categories: [""]
tags: ["Linux","docker"]
---

## 简介

[项目地址](https://github.com/fcwu/docker-ubuntu-vnc-desktop)

docker-ubuntu-vnc-desktop is a Docker image to provide web VNC interface to access Ubuntu LXDE/LxQT desktop environment.

对内存的占用非常大，推荐至少1G以上的服务器使用。另外，我这里的情况是可以开其他软件，但是打不开浏览器，不知道为什么。

## 使用

运行以下命令
```
docker run -p 6080:80 -v /dev/shm:/dev/shm dorowu/ubuntu-desktop-lxde-vnc
```
访问http://ip:6080/即可，记得开端口。

![](2022-03-05-11-41-14.png)