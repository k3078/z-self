Serverstatus：一个轻量级的服务器状态探针，界面清爽，资源消耗少。

![image-20211128095129306](image-20211128095129306.png)

## 说明

**推荐有能力的朋友使用哪吒面板，可自定义界面。支持一键配置受控端、网页终端连接、报警推送、VPS分组、自动识别位置等。**

`server`端必须为`Linux`，`client`端由于使用`python psutil`库，理论上只要能运行`python`就可以安装。

项目地址：

- [cokemine/ServerStatus-Hotaru: 云探针、多服务器探针、云监控、多服务器云监控 (github.com)](https://github.com/CokeMine/ServerStatus-Hotaru)

## 安装与使用（Linux）

```shell
# 注意使用sudo

#获取脚本
wget https://raw.githubusercontent.com/cokemine/ServerStatus-Hotaru/master/status.sh

# wget https://cokemine.coding.net/p/hotarunet/d/ServerStatus-Hotaru/git/raw/master/status.sh 若服务器位于中国大陆建议选择Coding.net仓库

#使用服务端配置启动脚本
bash status.sh s

#使用客户端配置启动脚本：
bash status.sh c
```

## 安装与使用（windows）

首先安装python（自行百度），然后安装依赖库：

```
python pip install psutil
```

或者

```
pip install psutil
```

下载通用的`py`文件[下载地址](https://raw.githubusercontent.com/cokemine/ServerStatus-Hotaru/master/clients/status-psutil.py)

转到你刚刚下载的目录中，修改以下内容为你自己的信息： [![4UByP1.png](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/4UByP1.png)](https://imgtu.com/i/4UByP1)

此处的信息要和你服务端添加的信息相同。

然后在此处打开cmd（上方地址栏键入cmd回车），开启服务:

```python
python status-psutil.py
```

## 和宝塔面板共存

如果已经安装有宝塔面板，则在安装过程中就应该取消使用脚本自动配置HTTPS服务。

![image-20220416130914083](image-20220416130914083.png)

直接在宝塔面板内新建网站到上述目录会报错。所以必须更改根目录。

以`/www/serverstatus`为例。

首先将`/usr/local/ServerStatus/web`内的文件复制到`/www/serverstatus`中

修改服务器脚本文件：

```shell
nano /etc/init.d/status-server
```

![image-20220416130918937](image-20220416130918937.png)

重启`Serverstatus`。

然后转到宝塔面板，新增网页到对应目录即可。

![image-20220416130924148](image-20220416130924148.png)

## 自定义域名（自动安装情况下）

如果是使用它自身的网站服务，搭建好后要添加其他网站或者更改域名怎么办呢？

分析脚本，可以发现本项目使用`Caddy`搭建网站服务。

转到`caddy`配置文件所在位置，一般是`user/local/caddy`，修改这个目录下的`Caddyfile`即可。详细操作可以查看`caddy`相关文档，下图展示了绑定status.kersite.ga的配置方式：

![image-20220416130851610](image-20220416130851610.png)

## 数据迁移

手动配置信息还是比较慢的，何况有时候切换探针服务端所在VPS，就要把已经录好的信息重新输一遍，更加令人抓狂。实际上我们可以直接复制配置文件达到这个效果。

如图，直接复制`/usr/local/ServerStatus/server`目录下的`config.json`文件到对应位置。

> 我这里使用的软件是termius

![image-20220416130825991](image-20220416130825991.png)

此时客户端上的配置还是旧的，所以全部显示维护中。

如果你的服务端IP地址换了，客户端也要进行相应更改，也可以直接编辑配置文件来修改服务端IP。

```shell
nano /usr/local/ServerStatus/server/client/status-client.py
```

修改对应位置，再重启客户端即可：

![img](https://dev.kermsite.com/p/serverstatus/2022-01-14-12-33-03.png)

## 附录

### 参考文章

### 版权信息

本文原载于kermsite.com，复制请保留原文出处。

