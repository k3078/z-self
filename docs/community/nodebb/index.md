---
title: "使用 Docker 快速搭建 Nodebb"
slug: "Nodebb"
date: 2022-06-24T09:56:02+08:00
draft: false
---

Node.js based forum software built for the modern web

## 效果

![](2022-06-24-09-57-15.png)

## 准备工作

1. VPS （实验环境：2H4G，ubuntu 20.04）
2. 域名

## 详细教程

### 安装 docker。

```sh
apt install curl
curl -fsSL https://get.docker.com | bash -s docker
```

### 部署数据库和网站

```sh
docker network create mongo-net
docker run --name mongo --restart always --network mongo-net -d mongo --wiredTigerCacheSizeGB 0.8 这个0.8是默认的数据库内存占用（0.8G），亲测设的很小会导致频繁重启。推荐设到0.5以上（看你 VPS 配置）
docker run --restart always --name forum --network mongo-net -p 4567:4567 -d nodebb/docker
```

此时网站已在 4567 端口运行

### 初始化网站

访问 `ip:4567`，如图配置。

![](2022-06-24-10-04-41.png)

等待配置完成（一段时间）。另外不要着急，这个初始化挺慢的，配置完成之后，再等他清下缓存之后登录会比较靠谱点。

![](2022-06-24-10-05-04.png)

### 安装并配置 Caddy 反代（可选）

```sh
sudo apt install -y debian-keyring debian-archive-keyring apt-transport-https
curl -1sLf 'https://dl.cloudsmith.io/public/caddy/stable/gpg.key' | sudo gpg --dearmor -o /usr/share/keyrings/caddy-stable-archive-keyring.gpg
curl -1sLf 'https://dl.cloudsmith.io/public/caddy/stable/debian.deb.txt' | sudo tee /etc/apt/sources.list.d/caddy-stable.list
sudo apt update
sudo apt install caddy
```

`nano /etc/caddy/Caddyfile` 写入：

```
# The Caddyfile is an easy way to configure your Caddy web server.
#
# Unless the file starts with a global options block, the first
# uncommented line is always the address of your site.
#
# To use your own domain name (with automatic HTTPS), first make
# sure your domain's A/AAAA DNS records are properly pointed to
# this machine's public IP, then replace ":80" below with your
# domain name.

:80 {
        # Set this path to your site's directory.
        root * /usr/share/caddy

        # Enable the static file server.
        file_server

        # Another common task is to set up a reverse proxy:
        # reverse_proxy localhost:8080

        # Or serve a PHP site through php-fpm:
        # php_fastcgi localhost:9000
}

# 上面默认不用改

xxx.com {
        tls kerm9273@outlook.com 改成你的邮箱，用来申请 https
        reverse_proxy localhost:4567
}
```

请先设置好DNS解析，再重启 caddy。如果使用 Cloudflare 需要关掉小云朵。等待一会再重启。**否则可能出现证书申请错误，网站打不开。**

```
service caddy restart
```

大功告成！访问你设置好的域名即可。


## Reference

1. [快速搭建论坛，用 NodeBB 搭建自己的社区，汇聚可爱的人们](https://www.jianshu.com/p/ab72fe404c8d)
2. [Home | NodeBB | Your Community Forum Platform](https://nodebb.org/)