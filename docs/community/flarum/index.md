---
title: "使用 Docker 快速搭建 Flarum"
slug: "Flarum"
date: 2022-06-24T09:38:01+08:00
draft: false
---

Flarum 是一款优雅简洁论坛软件。让在线交流变得更加轻松愉快

## 效果

![](2022-06-24-09-52-45.png)

## 准备工作

1. VPS （实验环境：1H1G openVZ VPS，Ubuntu 20.04）
2. 一个域名

## 详细教程

以下为本人安装的全过程，可按需更改。

安装 docker，新建文件夹，新建并修改 docker 配置文件

```sh
apt install curl
curl -fsSL https://get.docker.com | bash -s docker
apt install docker-compose -y
mkdir flarum
cd flarum
```

`nano docker-compose.yaml` 写入以下内容：

```yml
version: "3"

services:
  flarum:
    image: mondedie/flarum:stable
    container_name: flarum
    env_file:
      - /mnt/docker/flarum/flarum.env
    volumes:
      - /mnt/docker/flarum/assets:/flarum/app/public/assets
      - /mnt/docker/flarum/extensions:/flarum/app/extensions
      - /mnt/docker/flarum/storage/logs:/flarum/app/storage/logs
      - /mnt/docker/flarum/nginx:/etc/nginx/flarum
    ports:
      - 8080:8888
    depends_on:
      - mariadb

  mariadb:
    image: mariadb:10.5
    container_name: mariadb
    environment:
      - MYSQL_ROOT_PASSWORD=123456
      - MYSQL_DATABASE=flarum
      - MYSQL_USER=flarum
      - MYSQL_PASSWORD=123456
    volumes:
      - /mnt/docker/mysql/db:/var/lib/mysq
```

新建并修改 flarum 配置文件

```sh
mkdir /mnt/docker/flarum
mkdir -p /mnt/docker/flarum
```

`nano /mnt/docker/flarum/flarum.env` 写入以下内容

```env
DEBUG=false
FORUM_URL=https://xxx.com 修改为网站域名

# Database configuration
DB_HOST=mariadb
DB_NAME=flarum
DB_USER=flarum
DB_PASS=123456    
DB_PREF=flarum_
DB_PORT=3306

# User admin flarum (environment variable for first installation)
# /!\ admin password must contain at least 8 characters /!\
FLARUM_ADMIN_USER=kerm 修改为管理员用户名
FLARUM_ADMIN_PASS=123456 修改为管理员密码 
FLARUM_ADMIN_MAIL=admin@domain.tld
FLARUM_TITLE=BGM 修改为站点名称
```

开始部署

```sh
docker-compose up -d
```

使用 Caddy 配置域名反代（注意必须使用上面给定的站点域名访问，否则报错）

安装并修改 Caddy 配置文件。

```sh
sudo apt install -y debian-keyring debian-archive-keyring apt-transport-https
curl -1sLf 'https://dl.cloudsmith.io/public/caddy/stable/gpg.key' | sudo gpg --dearmor -o /usr/share/keyrings/caddy-stable-archive-keyring.gpg
curl -1sLf 'https://dl.cloudsmith.io/public/caddy/stable/debian.deb.txt' | sudo tee /etc/apt/sources.list.d/caddy-stable.list
sudo apt update
sudo apt install caddy
```

`nano /etc/caddy/Caddyfile` 写入以下内容

```
# The Caddyfile is an easy way to configure your Caddy web server.
#
# Unless the file starts with a global options block, the first
# uncommented line is always the address of your site.
#
# To use your own domain name (with automatic HTTPS), first make
# sure your domain's A/AAAA DNS records are properly pointed to
# this machine's public IP, then replace ":80" below with your
# domain name.

:80 {
        # Set this path to your site's directory.
        root * /usr/share/caddy

        # Enable the static file server.
        file_server

        # Another common task is to set up a reverse proxy:
        # reverse_proxy localhost:8080

        # Or serve a PHP site through php-fpm:
        # php_fastcgi localhost:9000
}

# 上面默认不用改

xxx.com {
        tls kerm9273@outlook.com 改成你的邮箱，用来申请 https
        reverse_proxy localhost:8080
}
```

请先设置好DNS解析，再重启 caddy。如果使用 Cloudflare 需要关掉小云朵。等待一会再重启。**否则可能出现证书申请错误，网站打不开。**

```
service caddy restart
```

大功告成！访问你设置好的域名即可。

PS：汉化

```
docker exec -ti flarum extension require flarum-lang/chinese-simplified
```

使用管理员账户登录后台，左边导航栏里最下边会多出来一个汉化包，点击启用即可。
在常规设置里，可以修改默认的语言。

## Reference

1. [用docker快速搭建flarum论坛_一只渣渣程序猿的博客-CSDN博客_docker flarum](https://blog.csdn.net/qq_31452291/article/details/119842794)
2. [GitHub - mondediefr/docker-flarum: Docker image of Flarum](https://github.com/mondediefr/docker-flarum)