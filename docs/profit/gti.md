国内挂机项目，不能规模经营。聊胜于无。

![image-20220416120350626](image-20220416120350626.png)

## 说明

- IP要求：独立IPv4。**国内外均可**。推荐使用国内。
- 平台要求：Linux平台支持。使用脚本进行安装，支持centos、ubuntu、debian。
- 预估流量与收益：每台机器**每天大概0.3元。**
- 机器限制：授权模式，每个账号，**后台只有免费一台授权**，多挂无效。授权30天，99元。
- 出金限制与方式：兑换实物，或京东卡，支持10、100面额的京东卡。
- 原理：目前尚不清楚其原理。不过总的来说资源占用极小。蚊子腿也是肉。聊胜于无。

## 使用说明

### 注册小程序

首先打开微信扫码（带AFF）

![image-20220416115622205](image-20220416115622205.png)

然后点 我的 登录 绑定自己手机号

### 注册网站

打开 [猎报安全 (liebaoanquan.com)](https://www.liebaoanquan.com/login?register)

用**刚才绑定的手机号**注册账户 。

### 安装软件

参考 [安装下载 (liebaoanquan.com)](https://www.liebaoanquan.com/package/linux) 进行安装，可以根据系统在自己服务器上直接执行脚本。然后输入自己的账户密码就可以了。第二天打开小程序 查看收益

附debian脚本：

```
sudo apt-get install alien fakeroot rpm -y
wget https://vkceyugu.cdn.bspapp.com/VKCEYUGU-4317afbb-8ea4-49db-8e55-fbdde939d2a8/f9376567-c3e2-4882-9139-58a9ac0cd85c.deb
dpkg -i f9376567-c3e2-4882-9139-58a9ac0cd85c.deb
wget -O agentdInit.sh https://www.liebaoanquan.com/edrClient/download/agentdInit.sh --no-check-certificate && bash agentdInit.sh start
```



参考：

[闲置vps挂机每天1元 占用为百分之1-美国VPS综合讨论-全球主机交流论坛 - Powered by Discuz! (hostloc.com)](https://hostloc.com/thread-989130-1-1.html)