income是另外一个国外流量挂机项目。只支持Windows，出金门槛较高。

## 说明

- IP要求：独立国外IPv4。支持机房IP。国内IP基本没有流量。
- 平台要求：**只支持Windows**
- 出金限制与方式：注册送5$，但是最低提现额度是20$。提现方式为PayPal。
- 收益：注册送5$，在线收益每天0.06$，聊胜于无。流量收益=1GB=0.1 USD。流量收益不稳定。我这里挂了4台国外Windows服务器。作为参考。
- AFF：未研究

注册链接（带AFF）：https://income.spider.dev/r/mailjfncwk

## Windows安装

软件下载链接：https://f002.backblazeb2.com/file/earn-app/win/x86/IncomeSetup_v2.1.7.exe

安装后会打开浏览器界面，登录即可。有时会报错，不用管他，在web管理界面有收益就行。

![](2022-04-16-11-39-49.png)



