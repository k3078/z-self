Peer2Profit算是诸多挂机项目中门槛比较低，出金比较靠谱的一个。

## 说明

- IP要求：独立国外IPv4。支持机房IP。国内IP基本没有流量。越靠近俄罗斯，一般而言流量越大。纯IPv6服务器不能使用。
- 平台要求：全平台支持。Windows下使用软件安装，Linux下使用docker安装。
- 预估流量与收益：每月大概使用500MB流量（视位置而定），流量价格为0.3$/G。目前我的收益大概为每月2$（我挂了大概10台左右）。只能说是聊胜于无。
- 出金限制与方式：可以通过银行卡、Payeer、Yoomoney、加密货币出金。这里推荐使用加密货币+coinbase，最低出金额度为2$。直接到账。
- AFF：aff收益极高，这也是许多地方都有peer2profit推广的原因。

注册链接（带AFF）：https://peer2profit.com/r/164082340061ccfa682a5ae/en

![](2022-04-16-11-33-03.png)

## Linux安装

安装docker

```
sudo curl -sSL get.docker.com | sh
```

将以下邮箱改成你自己的运行：

```
export P2P_EMAIL=kerm9273@outlook.com; 
docker rm -f peer2profit || true && docker run -d --restart always \
        -e "P2P_EMAIL" \
        --name peer2profit \
        peer2profit/peer2profit_x86_64:latest 
```

## Windows安装

下载软件，直接安装运行即可：https://updates.peer2profit.app/Peer2Profit-Setup_0_45_signed.zip

## 附录

### 参考文章

1. [Peer2Profit挂机不间断赚钱项目及教程 - 乐云主机笔记 (letcloud.cn)](https://www.letcloud.cn/2337.html)
2. [Peer2Profit 挂机赚钱教程 - bujj博客 - www.bujj.org](https://www.bujj.org/index.php/2021/12/13/217/)
