## 介绍

利用vps搭建一个个人离线下载站。

- 充分利用OneDrive在国外的高速度，拓展vps的存储容量。
- 结合自动化流工具，实现自动追番、推送下载。
- 结合Emby等工具，实现在线观看OneDrive中视频，无需保存至本地。
- 结合浏览器脚本，实现百度网盘文件的转存下载。再也不用受百度客户端折磨。
- 对于速度好的文件，例如做种多的视频，可以充分利用VPS的千兆带宽，实现80M/s的下载速度。
- 对于速度差的文件，可以挂在VPS上，24小时不间断持续下载。

材料：一台VPS，带宽大流量多者优先，一个OneDrive账号，最好5T及以上，个人那个15G的就不要拿出来用了。（申请OneDrive 5T账号参考：[Microsoft 365 E5：开发人员订阅 (kermsite.com)](https://kermsite.com/p/mse5/)）

> 我是用之前申请的Digital ocean一年100刀的学生额度，开了一个小服务器，虽然小，但是带宽大、流量多，非常适合作离线下载工具
>
> DG对于新用户绑卡即送60天有效的100USD，挺良心的。不过但看价格确实没有竞争力，可以考虑用完优惠就走。邀请连接：https://m.do.co/c/dfdb5565bd04

实验环境：centos7、ubuntu20.14

## 安装Aria2

此处安装aria2使用[P3TERX的一键安装脚本（自带上传功能）](https://p3terx.com/archives/offline-download-of-onedrive-gdrive.html)

在SSH运行下面的代码：  

    sudo yum -y install wget //安装wget
    wget -N git.io/aria2.sh && chmod +x aria2.sh && ./aria2.sh

> (2021.9.22更新)如果git.io链接失败，可尝试使用以下链接wget https://sharelist-kerm.herokuapp.com/share/soft/github/aria2%E5%AE%89%E8%A3%85/aria2.sh && chmod +x aria2.sh && ./aria2.sh

即可自动安装aira2  

等待安装完成后，记下密钥，配置远程下载要用

此外，可以再次输入

    ./aria2.sh

打开脚本查看和修改aira2配置

## 利用AriaNG可视化（可选）

> 可以直接使用aria安装脚本提供的链接进行访问。

搭建宝塔面板，[官网](https://www.bt.cn/download/linux.html)  

在宝塔面板中新建网站，转到网站根目录，删除所有文件  

访问AriaNG的[发布地址](https://github.com/mayswind/AriaNg/releases)，下载最新版本的AllinOne文件，并上传到网站根目录（或者直接用宝塔的远程下载）  

此时访问网站绑定的域名应当能够看到AriaNG的界面，否则，检查服务器提供商处是否开启80端口

在宝塔面板和服务器商两处都开启6800端口（airaNG和aria2之间的通信端口）

ariaNG中按下图配置：
![](https://cdn.jsdelivr.net/gh/kerms0/pics/img/c07c4b17ffe345a68f922495f90e820e.png)

## 安装和配置Rclone

为了完成对rclone的配置，本地需要安装一个rclone，[官网](https://rclone.org/downloads/)

下载对应文件，解压，进入文件夹，在资源管理器地址栏输入cmd，回车就会在当前路径打开命令提示符。替换以下命令中的`Client_ID`、`Client_secret`并执行：

    rclone authorize "onedrive" "Client_ID" "Client_secret"

接下来会弹出浏览器，要求你登录账号进行授权。授权完后命令提示符窗口会出现以下信息：

    If your browser doesn't open automatically go to the following link: http://127.0.0.1:53682/auth
    Log in and authorize rclone for access
    Waiting for code...
    Got code
    Paste the following into your remote machine --->
    {"access_token":"xxxxxxxx"}  
    <---End paste

此时复制{xxxxxxxx}整个内容，并保存好，后面需要用到

服务器端，使用官方的一键安装脚本

    curl https://rclone.org/install.sh | sudo bash

安装完成之后，输入

    rclone config

开始配置，全程都有引导，选择n新建，名字随便起，例如onedrive，网盘种类选择onedrive，之后一路回车，**到**

    Use auto config?
    * Say Y if not sure
    * Say N if you are working on a remote or headless machine
    y) Yes
    n) No
    y/n> n

**的时候选择n**  

然后会弹出

    For this to work, you will need rclone available on a machine that has a web browser available.
    Execute the following on your machine:
        rclone authorize "onedrive"
    Then paste the result below:
    result> {"XXXXXXXX"} 

将之前复制的结果贴到result后面 （不要依照它的提示再去windows执行，会报错）

之后选1，按照实际情况选择编号，确认设置，按q退出

此时OneDrive已经设置好了，接下来是将其挂载到VPS上，安装fuse（依赖软件）

    yum install -y fuse

然后新建一个文件夹作为你挂载的目标，例如

    mkdir /home/onedrive

那么我的挂载命令就是：

    rclone mount onedrive: /home/onedrive --allow-other --allow-non-empty --vfs-cache-mode writes

完成之后可以关闭会话。

> rclone挂载命令支持许多自定义参数，上面提供的命令是最简单的，却不一定是最优的。
>
> 增大缓存可以提高挂载硬盘的读写能力，参考：
>
> [rclone 挂载优化~ | 垃圾站 (cyhour.com)](https://cyhour.com/1594/)
>
> [在Linux上使用rclone挂载Google Drive 和 Onedrive_内德-CSDN博客](https://blog.csdn.net/Nedved_L/article/details/101429006)

经过一番摸索，我目前使用的挂载配置为：

```
rclone mount onedrive: /home/onedrive --allow-other --attr-timeout 5m --vfs-cache-mode full --vfs-cache-max-age 24h --vfs-cache-max-size 10G --vfs-read-chunk-size-limit 100M --buffer-size 100M --daemon
```

## 配置aria2下载后自动上传至OneDrive

（注意此处是下载完成后自动上传，故一次性不能下大于硬盘容量的文件）

重新开启一个会话，输入

    nano /root/.aria2c/aria2.conf

打开 Aria2 配置文件进行修改。找到“下载完成后执行的命令”，把clean.sh替换为upload.sh。

    # 下载完成后执行的命令
    on-download-complete=/root/.aria2c/upload.sh

输入

    nano /root/.aria2c/script.conf

打开附加功能脚本配置文件进行修改，有中文注释，按照自己的实际情况进行修改，第一次使用只建议修改网盘名称。

    # 网盘名称(RCLONE 配置时填写的 name)
    drive-name=onedrive

重启 Aria2 。脚本选项重启或者执行以下命令：

    service aria2 restart

## 附注

用了一段时间发现经常要重新填写服务器配置。查了下文档发现是支持把配置写在链接里面的，修改以下链接：

```
http(s)://web前端地址/#!/settings/rpc/set/http/你的后端服务器ip或域名，如127.0.0.1/端口如6800/jsonrpc/密钥
```

这样访问这个链接就填写好配置了，但是要注意不要泄露这个网址。

参考：[AriaNg (mayswind.net)](http://ariang.mayswind.net/zh_Hans/command-api.html)

> 实际上，使用一键脚本配置完成之后也会反馈一个链接，将域名改成你的web前端域名即可。

## 附录

### 参考文章

### 版权信息

本文原载于kermsite.com，复制请保留原文出处。

