---
title: "Miniflux: 简洁的 RSS 阅读器"
slug: "Miniflux"
date: 2022-06-16T18:14:50+08:00
draft: false
---

之前部署在 Railway 的 TTRSS 挂了一次，数据全没了 🥲。还是放在自己的服务器上比较放心。

用了很久 TTRSS 也想换个口味，这次就用 Miniflux。

优势：

- 界面简洁，功能齐全。
- PWA，可直接作为应用安装。

## 截图

Web

![](2022-06-16-18-26-35.png)

安卓

![](2022-06-16-18-28-35.png)

## 部署

文件夹新建`docker-compose.yml`

写入：

```yml
version: '3.4'
services:
  miniflux:
    image: miniflux/miniflux:latest
    ports:
      - "8087:8080"
    depends_on:
      - db
    environment:
      - DATABASE_URL=postgres://miniflux:secret@db/miniflux?sslmode=disable
      - RUN_MIGRATIONS=1
      - CREATE_ADMIN=1
      - ADMIN_USERNAME=admin
      - ADMIN_PASSWORD=admin
  db:
    image: postgres:latest
    environment:
      - POSTGRES_USER=miniflux
      - POSTGRES_PASSWORD=secret
    volumes:
      - miniflux-db:/var/lib/postgresql/data
    healthcheck:
      test: ["CMD", "pg_isready", "-U", "miniflux"]
      interval: 10s
      start_period: 30s
volumes:
  miniflux-db:
```

然后运行`docker-compose up -d`即可。

默认端口 8087，用户名 admin，密码 admin。