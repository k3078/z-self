---
title: "Wallabag：稍后阅读软件"
slug: "Wallabag"
date: 2022-05-04T22:46:57+08:00
draft: false
---

### wallabag（通过docker-run）

- 项目地址：[Save the web, freely | wallabag: a self hostable application for saving web pages](https://wallabag.org/en)

- 简介：一款稍后阅读软件，有比较简洁的安卓和web端。可以自动离线。支持中文。可以把你想看的文章放在里面，多平台同步阅读。

  > Save and classify articles. Read them later. Freely.

- 安装：
  注意将其中`http://example.com`修改成你自己的域名

```shell
docker run -v /opt/wallabag/data:/var/www/wallabag/data -v /opt/wallabag/images:/var/www/wallabag/web/assets/images -p 800:80 -e SYMFONY__ENV__DOMAIN_NAME=http://example.com -d wallabag/wallabag
```

默认用户名和密码：`wallabag:wallabag`，默认监听800端口。
如想使用域名访问，参照上面（Awesome TTRSS）设置。