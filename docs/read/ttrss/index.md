---
title: "Docker一键部署TTRSS阅读器"
slug: "Ttrss"
date: 2022-05-04T22:47:58+08:00
draft: false
tags: ["docker"]
---

### Awesome TTRSS（通过docker-compose）

- 官网：- [Awesome TTRSS](http://ttrss.henry.wang/zh/)
- 简介：Tiny Tiny RSS是一款基于 PHP 的免费开源 RSS 聚合阅读器。Awesome TTRSS 旨在提供一个 「一站式容器化」 的 Tiny Tiny RSS 解决方案，通过提供简易的部署方式以及一些额外插件，以提升用户体验。支持中文。
- 效果演示：

![image-20211103212101473](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211103212101473.png)

- 安装方式：

```shell
apt-get install docker-compose
wget https://raw.githubusercontent.com/HenryQW/Awesome-TTRSS/main/docker-compose.yml //下载配置文件
nano docker-compose.yml //编辑配置文件
```

  此时会打开nano的编辑界面，如图所示，第一处修改成你的网站域名，第二三处修改成你自己的密码（注意要保持相同）

  [![hXoer6.png](https://z3.ax1x.com/2021/09/10/hXoer6.png)](https://imgtu.com/i/hXoer6)

  然后启动容器即可：

```shell
docker-compose up -d
```

  默认通过 181 端口访问 TTRSS，默认账户：admin 密码：password，请第一时间更改。
  如想使用域名访问，在部署之后，进入宝塔面板，建立新站点，反代127.0.0.1:181即可。注意域名要和你之前设置的相同。