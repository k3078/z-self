由于关于VPN搭建的内容较为繁杂，此处不做具体介绍，如有需要可自行搜索v2ray等。推荐使用mack-a脚本。


```
wget -P /root -N --no-check-certificate "https://raw.githubusercontent.com/mack-a/v2ray-agent/master/install.sh" && chmod 700 /root/install.sh && /root/install.sh
```