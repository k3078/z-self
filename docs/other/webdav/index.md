---
title: "Webdav文件分享探究"
slug: "Webdav"
date: 2022-03-18T23:27:30+08:00
draft: false
description: ""
categories: [""]
tags: ["mark"]
---

webdav是一个文件分享协议，比起ftp安全，并且容易配置。

基于此协议开发出了许多软件。比较常见的有CHFS、webdav小秘等，这两个软件都有图形界面，配置方便，但都不是开源软件，网上甚至有试用chfs中病毒的情况。出于安全考虑这里就不使用这两款软件了。

CHFS参考：https://www.iplaysoft.com/chfs.html

webdav小秘参考：https://lightzhan.xyz/index.php/webdavhelper/


我们用到的是GitHub上的一款开源命令行webdav服务工具。

[项目地址](https://github.com/hacdias/webdav)

[下载地址](https://github.com/hacdias/webdav/releases/download/v4.1.1/windows-amd64-webdav.zip)

使用说明：

下载软件，在同一目录下新建配置文件`config.yaml`，输入以下配置：

```yaml
address: 0.0.0.0
port: 10170 #端口
auth: true
tls: false
cert: cert.pem
key: key.pem
prefix: /
debug: false

scope: .
modify: true
rules: []

users:
  - username: kerm # 用户名
    password: password #密码
    scope: C:/share #文件共享目录的绝对地址
```

修改以上四处，其他保持默认即可。在当前文件夹下打开cmd，输入

```
webdav -c ./config.yaml
```

即可运行。记得打开端口。

## 链接

电脑上，不推荐使用Windows自带的映射网络驱动（各种各样的bug），推荐使用rclone。

[windows似乎只支持默认端口的webDav地址 ，要么用NETDRIVE2，如果只为看片potplayer支持webDav，亲测比NETDRIVE2流畅](http://www.gebi1.com/thread-263297-1-1.html)

另外，直接访问ip:port，正常情况下也有输出：

![](2022-03-19-09-54-20.png)

使用[winscp](https://winscp.net/eng/docs/lang:chs)进行连接和管理也是可行的

## Linux上使用apache配置

IOS链接

https://segmentfault.com/a/1190000018015755