### sharelist（通过docker-run）

- 项目地址：[安装 (reruin.github.io)](https://reruin.github.io/sharelist/docs/v0.1/#/zh-cn/installation)
- 简介：ShareList 是一个易用的网盘工具，支持快速挂载 GoogleDrive、OneDrive ，可通过插件扩展功能。
- 安装方式：

```shell
docker run -d -v /etc/sharelist:/sharelist/cache -p 33001:33001 --name="sharelist" reruin/sharelist
```

默认监听33001端口。