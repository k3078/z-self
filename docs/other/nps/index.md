---
title: "docker部署nps、npc"
slug: "nps"
date: 2022-02-16T15:17:18+08:00
draft: true
description: ""
categories: [""]
tags: ["内网穿透"]
---

### 介绍：

一款轻量级、功能强大的内网穿透代理服务器。支持tcp、udp流量转发，支持内网http代理、内网socks5代理，同时支持snappy压缩、站点保护、加密传输、多路复用、header修改等。支持web图形化管理，集成多用户模式。

### 使用方式

可自行参考[官方配置][1]

也可以依照我的操作:

    mkdir /home/nps
    cd /home/nps
    wget https://sharelist-kerm.herokuapp.com/share/soft/github/nps/conf.zip
    unzip conf.zip
    docker run -d --name nps -p 10180:8080 -p 10124:8024 -p 10150-10179:10150-10179 -v /home/nps/conf:/conf ffdfgdfg/nps

查看日志

    docker logs nps

初始登录账户密码：admin：123（当然也可以自己更改nps.conf）

此处和原版主要有两个不同：
1. 修改了conf文件下载地址，改成了部署在heroku上的个人服务器，比较方便。
2. 没有直接将docker的net接到宿主机上，只开放了10000之后的几个端口。**注意要相应在宿主机和服务商处打开端口10180、10124、10150-10179**，**客户端配置端口为10124**，**穿透时端口选择10150-10179**。这样做主要是防止端口冲突（比如80和443）


### 参考

 [官方文档][1]

### 下载

    wget 
    tar -zxvf <你刚才下载的文件名>

### 直接使用server配置

使用以下命令

     ./npc -server=<你的ip:port> -vkey=<webserver中显示的客户端key>

### 使用conf文件配置并添加到系统服务

实际上只要配置ip和port就可以了。会显示使用公钥

    nano conf/npc.conf

对于linux、darwin

注册：sudo ./npc install 其他参数（例如-server=xx -vkey=xx或者-config=xxx）
启动：sudo npc start
停止：sudo npc stop
如果需要更换命令内容需要先卸载./npc uninstall，再重新注册

[![4wzOSg.png](https://z3.ax1x.com/2021/09/23/4wzOSg.png)](https://imgtu.com/i/4wzOSg)
[![4wzzmn.png](https://z3.ax1x.com/2021/09/23/4wzzmn.png)](https://imgtu.com/i/4wzzmn)

### 后台运行

直接使用screen：[参看另一篇文章][2]


  [1]: https://ehang-io.github.io/nps/#/
  [2]: http://blog.kermsite.ml/index.php/archives/62/


  [1]: https://hub.docker.com/r/ffdfgdfg/nps