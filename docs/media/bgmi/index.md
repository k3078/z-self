---
title: "Bgmi"
description: "Bgmi"
slug: "Bgmi"
date: 2022-07-07T10:46:35+08:00
draft: false
---


以下是我配置的全部过程

```bash
# 获取所有连载番剧
bgmi cal

# 订阅番剧。直接复制番剧名即可，Overlord 报错
bgmi add "夏日重现" "就算这样，“步”还是靠了过来" "Lycoris Recoil" "Engage Kiss" "RWBY 冰雪帝国" "OVERLORD Ⅳ " "继母的拖油瓶是我的前女友" "异世界归来的舅舅"

# 由于 Overlord 报错，重新把后面的两部先添加
bgmi add "继母的拖油瓶是我的前女友" "异世界归来的舅舅"

# 再次尝试无果，放弃
bgmi add "OVERLORD Ⅳ "

# 开始下载番剧
bgmi update --download

# 下载番剧封面
bgmi cal --download-cover
```