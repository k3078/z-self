---
title: "Emby搭建个人媒体库"
slug: "Emby"
date: 2022-05-04T22:46:18+08:00
draft: false
---

### 1. 简介：

emby（原名Media Browser）是一个主从式架构（客户端和服务器分离）的媒体服务器软件，可以用来整理服务器上的视频和音频，并将音频和视频流式传输到客户端设备。Emby服务器端支持Windows、Linux、MacOS、FreeBSD，客户端除上述平台以外，还支持HTML5网页，Android和IOS等移动操作系统。

emby同样支持Roku、Amazon Fire TV、Chromecast和Apple TV等流媒体设备，LG智能电视和三星智能电视等智能电视，以及Xbox 360和Xbox One等游戏机。Emby自带了非常多的插件，它可以帮助你搭建一个自己的影视媒体库，实现在线播放视频、音乐。

### 2. 下载和安装

官网提供了非常详尽的下载和安装方式：[Download Emby - Emby](https://emby.media/download.html)

简而言之，对于windows，你只需要下载软件运行即可。对于linux，部分发行版（如centos）提供了一键安装脚本，其他可以先下载再安装，当然最快的方式应该是docker。

安装完成之后，打开`ip:8096` 就可以访问web端了。emby不提供桌面的服务面板，而是统一在web端配置管理。刚开始用可能不习惯，但用多了绝对会说：真香。

打开网页之后会提示初始化，刚开始是英文的。这里就一路跟着引导走，创建管理员账号，一路默认，媒体库（library）可以先不添加，只不过要注意设置language的时候选择简体中文。

### 3. 配置媒体库

再次登录，界面就变成中文了。登录我们刚创建的账号：

![image-20211101133017042](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211101133017042.png)

然后右上角齿轮进入后台管理界面，点击媒体库、添加媒体库：

![image-20211101133242249](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211101133242249.png)

根据你自己的情况，选择内容类型和文件夹，对媒体库进行配置。

添加之后，等待扫描完毕，返回首页，就可以看到我们的媒体文件了。



参考：[EMBY自建个人影音播放系统-使用免费开源Emby打造个人影视媒体库 - 挖站否-挖掘建站的乐趣 (wzfou.com)](https://wzfou.com/emby/)