---
title: "VPS使用指南 简介"
slug: "use"
date: 2022-02-04T22:47:58+08:00
draft: false
---

本部分是 Zhelper 项目的一部分。

如果您希望参与到本文档的编辑中，或是对教程有不理解的地方，可以加入 [TG群](https://t.me/zhelper)（国内无法直接访问） 或 [论坛](https://bbs.zhelper.net/)

本部分的所有服务都需要使用SSH进行部署。即，**不适用于虚拟主机。**